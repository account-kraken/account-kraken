from . import gitlab, github

PROVIDERS = (gitlab.Group, github.Organization)


def get_choices():
    return [(p.provider_type, p.provider_desc) for p in PROVIDERS]


def get_provider(name):
    for provider in PROVIDERS:
        if provider.provider_type == name:
            return provider()

    return None
