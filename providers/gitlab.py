from django.conf import settings
from gitlab import Gitlab


class Provider:
    social_auth_provider = "gitlab"

    def __init__(self):
        token = settings.PROVIDER_GITLAB_TOKEN
        self.gl = Gitlab("https://gitlab.com", private_token=token)


class Group(Provider):
    provider_type = "gitlab.Group"
    provider_desc = "Group on gitlab.com"

    def get_members(self, id):

        group = self.gl.groups.get(id)

        return [m.id for m in group.members.list()]

    def __str__(self):
        return self.provider_type
