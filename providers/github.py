from django.conf import settings
from github import Github


class Provider:
    social_auth_provider = "github"

    def __init__(self):
        token = settings.PROVIDER_GITHUB_TOKEN
        self.gh = Github(token)


class Organization(Provider):
    provider_type = "github.Organization"
    provider_desc = "Organization on github.com"

    def get_members(self, id):

        org = self.gh.get_organization(id)

        return [m.id for m in org.get_members()]

    def add_member(self, id, user):
        org = self.gh.get_organization(id)
        member = self.gh.get_user_by_id(
            user.social_auth.get(provider=self.social_auth_provider).uid
            # TODO: There is no documented endpoint to fetch the user via id
            # however /user/:id seems to work but is not supported by PyGithub
            # as a fallback we use the login field from the extra_data
            # user.social_auth.get(provider=self.social_auth_provider).extra_data["login"]
        )

        org.add_to_members(member)

    def __str__(self):
        return self.provider_type
