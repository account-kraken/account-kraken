# Account Kraken

## Usage

This project uses [pipenv](https://github.com/pypa/pipenv) to manage it's dependencies.

1. Install package requirements
   ```shell
   pipenv install --deploy
   ```

1. Set the environment

    | Environment variable      | Required | Description                                    | Example value     | Setup instruction                                                                                                                                                                       |
    |---------------------------|----------|------------------------------------------------|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | DJANGO_SECRET_KEY         | yes      | This is used to provide  cryptographic signing | swdgfwgt43tgsgdfg | https://docs.djangoproject.com/en/2.1/ref/settings/#secret-key                                                                                                                          |
    | SOCIAL_AUTH_GITHUB_KEY    | yes      | OAuth2 Client ID                               | swdgfwgt43tgsgdfg | [Instructions](https://developer.github.com/apps/building-oauth-apps/creating-an-oauth-app/) Make sure to specifiy `http://127.0.0.1:8000/auth/complete/github/` as callback url        |
    | SOCIAL_AUTH_GITHUB_SECRET | yes      | OAuth2 Client Secret                           | swdgfwgt43tgsgdfg | See `SOCIAL_AUTH_GITHUB_KEY`                                                                                                                                                            |
    | SOCIAL_AUTH_GITLAB_KEY    | yes      | OAuth2 Client ID                               | fdsgfsdggffd      | [Instructions](https://docs.gitlab.com/ce/integration/oauth_provider.html) Make sure to specify `http://127.0.0.1:8000/auth/complete/gitlab/` as callback url and the scope `read_user` |
    | SOCIAL_AUTH_GITLAB_SECRET | yes      | OAuth2 Client Secret                           | swdgfwgt43tgsgdfg | See `SOCIAL_AUTH_GITLAB_KEY`                                                                                                                                                            |
    | SOCIAL_AUTH_CODE_KEY      | yes      | OAuth2 Client ID                               | N.A.              | N.A.                                                                                                                                                                                    |
    | SOCIAL_AUTH_CODE_SECRET   | yes      | OAuth2 Client Secret                           | N.A.              | N.A.                                                                                                                                                                                    |
    | PROVIDER_GITHUB_TOKEN     | yes      | Github token that is allowed to manage organization | N.A. | | |
    | PROVIDER_GITLAB_TOKEN     | yes      | Gitlab token that is allowed to manage group        | N.A. | | |

1. Run db migrations
   ```shell
   pipenv run migrate
   ```

1. Start the server
   ```
   pipenv run web
   ```

## Features

- [ ] Account Linker
  - [x] PoC
  - [ ] MVP
  - [ ] API to consume linked account information
- [ ] Group Syncer
  - [x] PoC
  - [ ] MVP
  - [x] Execute sync from gitlab.com to github.com

### Supported providers

- [x] GitHub
- [x] GitLab
- [ ] code.siemens.com
- [ ] Bitbucket
- [ ] docker.io
- [ ] NPM
- [ ] crates.io

## License

This is project is licensed under the [GNU Affero General Public License v3.0 or later](https://opensource.org/licenses/AGPL-3.0).
See `LICENSE` for details.
