Glossary
========

.. glossary::

    Account Kraken
        Manage group membership accross multiple online services

    Provider
        An external service that has users and group like structures.
        Example of providers are:

        - Github
        - Gitlab
        - Docker Hub
        - NPM
        - etc.
