============
Architecture
============

The following gives an architecture overview according to the
`C4 Model <https://c4model.com/>`__.

System Context Diagram
======================

.. uml:: system_context.puml

Container Diagram
=================

.. uml:: container_kraken.puml

Component diagrams
==================

Account Linker
--------------

The acount linker is repsonsible for mapping user IDs from different :term:`providers <Provider>` to a common user.

.. uml:: component_account_linker.puml

Group Syncer
------------

The group syncer is responsible for mapping group like structures between different services.
It also performs changes to these structures to map from source to target groups.

.. uml:: component_group_syncer.puml
