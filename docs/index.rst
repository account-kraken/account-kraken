.. Account Kraken documentation master file, created by
   sphinx-quickstart on Thu Jun 13 22:42:49 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Account Kraken's documentation!
==========================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    arch/README
    glossary


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
