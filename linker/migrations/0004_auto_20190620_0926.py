# Generated by Django 2.2.2 on 2019-06-20 09:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('linker', '0003_auto_20190619_2149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usercollection',
            name='remote_type',
            field=models.CharField(choices=[('gitlab.Group', 'Group on gitlab.com'), ('github.Organization', 'Organization on github.com')], max_length=50),
        ),
        migrations.CreateModel(
            name='UserCollectionSync',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='linker.UserCollection')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='linker.UserCollection')),
            ],
        ),
    ]
