from django.urls import path, include
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path("", views.settings, name="account-settings"),
    path("test/", views.members_test),
    path("sync/", views.sync_list, name="sync"),
    path("sync/<int:id>/", views.sync_detail, name="sync-detail"),
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
]
