from django.db import models
from django.contrib.auth.models import AbstractUser
import providers


class User(AbstractUser):
    pass
    # TODO: allow user to keep member ship private (e.g. conceal memebership in github)


class UnknownUser:
    def __init__(self, id, provider):
        self.remote_id = id
        self.remote_provider = provider

    def __str__(self):
        return f"UnknownUser ({self.remote_id}) from {self.remote_provider}"


class UserCollection(models.Model):
    remote_id = models.CharField(max_length=200)

    remote_type = models.CharField(choices=providers.get_choices(), max_length=50)

    @property
    def provider(self):
        from providers import get_provider

        return get_provider(self.remote_type)

    def get_members(self):

        members = []
        for m in self.provider.get_members(self.remote_id):
            print(m)
            user = UnknownUser(m, self.provider)
            try:
                user = User.objects.filter(
                    social_auth__provider=self.provider.social_auth_provider
                ).get(social_auth__uid=m)
            except User.DoesNotExist:
                pass

            members.append(user)

        return members

    def add_members(self, add_list):
        # Filter out UnknownUsers they are only used for display purpose but can't be added
        add_list = [m for m in add_list if type(m) is User]
        print("Adding:", add_list)
        for m in add_list:
            self.provider.add_member(self.remote_id, m)

    def remove_members(self, remove_list):
        remove_list = [m for m in remove_list if type(m) is User]
        print("Removing:", remove_list)

    def __str__(self):
        return f"{self.remote_type} ({self.remote_id})"


class UserCollectionSync(models.Model):
    source = models.ForeignKey(
        UserCollection, on_delete=models.CASCADE, related_name="+"
    )
    target = models.ForeignKey(
        UserCollection, on_delete=models.CASCADE, related_name="+"
    )

    def diff_list(self):
        source = self.source.get_members()
        target = self.target.get_members()
        common_list = [user for user in source if user in target]
        add_list = [user for user in source if user not in target and user is not None]
        remove_list = [
            user for user in target if user not in source and user is not None
        ]

        return (common_list, add_list, remove_list)

    def __str__(self):
        return f"Sync: {self.source} -> {self.target}"
