from social_core.backends.gitlab import GitLabOAuth2

class CodeOAuth2(GitLabOAuth2):
    name = 'code'
    API_URL = 'https://code.siemens.com'
    AUTHORIZATION_URL = 'https://code.siemens.com/oauth/authorize'
    ACCESS_TOKEN_URL = 'https://code.siemens.com/oauth/token'
