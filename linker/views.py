from django.shortcuts import render

# Create your views here.
from django.contrib.auth.decorators import login_required

from social_django.models import UserSocialAuth

from .models import UserCollection, UserCollectionSync


@login_required
def settings(request):
    user = request.user

    try:
        github_login = user.social_auth.get(provider="github")
    except UserSocialAuth.DoesNotExist:
        github_login = None

    try:
        gitlab_login = user.social_auth.get(provider="gitlab")
    except UserSocialAuth.DoesNotExist:
        gitlab_login = None

    try:
        code_login = user.social_auth.get(provider="code")
    except UserSocialAuth.DoesNotExist:
        code_login = None

    can_disconnect = user.social_auth.count() > 1 or user.has_usable_password()

    print(gitlab_login, github_login)

    return render(
        request,
        "linker/settings.html",
        {
            "username": user.username,
            "github_login": github_login,
            "gitlab_login": gitlab_login,
            "code_login": code_login,
            "can_disconnect": can_disconnect,
        },
    )


def sync_list(request):
    syncs = UserCollectionSync.objects.all()

    print(syncs)

    return render(request, "linker/sync_list.html", {"syncs": syncs})


def sync_detail(request, id):
    sync = UserCollectionSync.objects.get(pk=id)

    (common, to_add, to_remove) = sync.diff_list()

    if request.method == "POST":
        sync.target.add_members(to_add)
        sync.target.remove_members(to_remove)

        (common, to_add, to_remove) = sync.diff_list()

    print(sync)
    return render(
        request,
        "linker/sync_detail.html",
        {"sync": sync, "common": common, "to_add": to_add, "to_remove": to_remove},
    )


def members_test(request):
    sync = UserCollectionSync.objects.all().first()
    gl_members = sync.target.get_members()
    gh_members = sync.source.get_members()

    (add_list, remove_list) = sync.diff_list()

    return render(
        request,
        "linker/members_test.html",
        {
            "gl": gl_members,
            "gh": gh_members,
            "to_add": add_list,
            "to_remove": remove_list,
        },
    )
