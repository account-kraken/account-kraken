from django.contrib import admin

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, UserCollection, UserCollectionSync

admin.site.register(User, UserAdmin)
admin.site.register(UserCollection)
admin.site.register(UserCollectionSync)
